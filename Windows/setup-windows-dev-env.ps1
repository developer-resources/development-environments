# Inspired by https://www.jamestharpe.com/automatic-development-environment-setup/
#-------------------------------------------------------------------------------#
#                                                                               #
# This script installs all things needed to develop with Windows.               #
#                                                                               #
# To install without downloading the file follow instructions under AutomateIt  #
# in the README.md file                                                         #
#                                                                               #
# To install manually, download the file, then:                                 #
# Run PowerShell with admin privs, type `setup-windows-dev-env`, then wait.     #
#                                                                               #
#-------------------------------------------------------------------------------#

#
# Functions
#

[Net.ServicePointManager]::SecurityProtocol = "tls12"

function Update-Environment-Path
{
    $env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") `
        + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")
}

# Allow scripts to be ran in PS
set-executionpolicy remotesigned

# Install Boxstarter & Chocolatey
. { iwr -useb https://boxstarter.org/bootstrapper.ps1 } | iex; get-boxstarter -Force

# Install Programs via Chocolatey
Install-BoxstarterPackage -PackageName https://bitbucket.org/developer-resources/development-environments/raw/HEAD/Windows/boxstarter

Update-Environment-Path

# Git Configuration
git config --global alias.pos 'pull origin staging'
git config --global alias.pom 'pull origin master'
git config --global alias.last 'log -1 HEAD'
git config --global alias.ls "log --pretty=format:'%C(yellow)%h %ad%Cred%d %Creset%s%Cblue [%cn]' --decorate --date=short"
git config --global alias.ammend "commit -a --amend"
git config --global alias.standup "log --since yesterday --author $(git config user.email) --pretty=short"
git config --global alias.everything "! git pull && git submodule update --init --recursive"
git config --global alias.aliases "config --get-regexp alias"

Update-Environment-Path

# Node 
npm install --global --production npm-windows-upgrade
npm-windows-upgrade --npm-version latest
npm install -g gulp-cli 

Update-Environment-Path

# Bower
npm install -g bower

# Grunt
npm install -g grunt-cli

# ESLint
npm install -g eslint
npm install -g babel-eslint

Update-Environment-Path

# Upgrade Chocolatey
choco upgrade all --yes

#
# Optional Manual Steps
#

#Switch easily between VirtualBox and Hyper-V with a BCDEdit boot Entry in Windows
#https://www.hanselman.com/blog/SwitchEasilyBetweenVirtualBoxAndHyperVWithABCDEditBootEntryInWindows81.aspx

Write-Output "Finished! Run `choco upgrade all` to get the latest software"