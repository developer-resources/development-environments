# 1-Click Development Environments

For a hassle free 1-Click setup of your developer host system click on your operating system listed below and then follow the instructions under AutomateIt: 

+ [Windows](https://bitbucket.org/developer-resources/development-environments/src/master/Windows/)
+ [Mac](https://bitbucket.org/developer-resources/development-environments/src/master/Mac/)